const studentJoe = {
  name: 'Joe',
  email: 'joe@mail.com',
  grades: [78, 82, 79, 85],

  // compute for their grade average
  average() {
    return this.grades.reduce((acc, cur) => acc + cur) / this.grades.length
  },

  // determine if student avg is >= 85
  willPass() {
    return this.average() >= 85 ? 'will pass' : false
  },

  // >=90 with true, >=85 & <90 false, <85 undefinded
  willPassWithHonors() {
    const avg = this.average()
    switch (true) {
      case avg >= 90:
        return true

      case avg >= 85 && avg < 90:
        return false

      case avg < 85:
        return undefined

      default:
        return 'error - average out of bound'
        break
    }
  },
}

const studentJane = {
  name: 'Jane',
  email: 'jane@mail.com',
  grades: [87, 89, 91, 93],

  // compute for their grade average
  average() {
    return this.grades.reduce((acc, cur) => acc + cur) / this.grades.length
  },

  // determine if student avg is >= 85
  willPass() {
    return this.average() >= 85 ? 'will pass' : false
  },

  // >=90 with true, >=85 & <90 false, <85 undefinded
  willPassWithHonors() {
    const avg = this.average()
    switch (true) {
      case avg >= 90:
        return true

      case avg >= 85 && avg < 90:
        return false

      case avg < 85:
        return undefined

      default:
        return 'error - average out of bound'
        break
    }
  },
}

const studentJessie = {
  name: 'Jessie',
  email: 'jessie@mail.com',
  grades: [91, 89, 92, 93],

  // compute for their grade average
  average() {
    return this.grades.reduce((acc, cur) => acc + cur) / this.grades.length
  },

  // determine if student avg is >= 85
  willPass() {
    return this.average() >= 85 ? 'will pass' : false
  },

  // >=90 with true, >=85 & <90 false, <85 undefinded
  willPassWithHonors() {
    const avg = this.average()
    switch (true) {
      case avg >= 90:
        return true

      case avg >= 85 && avg < 90:
        return false

      case avg < 85:
        return undefined

      default:
        return 'error - average out of bound'
        break
    }
  },
}

const studentJohn = {
  name: 'John',
  email: 'john@mail.com',
  grades: [100, 100, 100, 100],

  // compute for their grade average
  average() {
    return this.grades.reduce((acc, cur) => acc + cur) / this.grades.length
  },

  // determine if student avg is >= 85
  willPass() {
    return this.average() >= 85 ? 'will pass' : false
  },

  // >=90 with true, >=85 & <90 false, <85 undefinded
  willPassWithHonors() {
    const avg = this.average()
    switch (true) {
      case avg >= 90:
        return true

      case avg >= 85 && avg < 90:
        return false

      case avg < 85:
        return undefined

      default:
        return 'error - average out of bound'
        break
    }
  },
}

const classOf1A = {
  students: [studentJoe, studentJane, studentJessie, studentJohn],

  countHonorStudents() {
    let count = 0
    this.students.forEach((student) => {
      student.willPassWithHonors() && count++
    })
    return count
  },

  honorsPercentage() {
    return (this.countHonorStudents() / this.students.length) * 100
  },

  retrieveHonorStudentInfo() {
    return this.students
      .filter((student) => student.willPassWithHonors() === true)
      .map((student) => ({
        email: student.email,
        average: student.average(),
      }))
  },

  sortHonorStudentsByGradeDesc() {
    return this.retrieveHonorStudentInfo().sort((a, b) => b.average - a.average)
  },
}

console.log(classOf1A.countHonorStudents())
console.log(classOf1A.retrieveHonorStudentInfo())
console.log(classOf1A.sortHonorStudentsByGradeDesc())
